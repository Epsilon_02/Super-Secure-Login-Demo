package com.example.demoApplication;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ViewController {

    @RequestMapping("/")
    public String home() {
        System.out.println("Going home...");
        return "index";
    }

    @RequestMapping("/register")
    public String showRegisterForm(Model model) {
        User user = new User();

        model.addAttribute("user", user);

        return "register_form";
    }

    @PostMapping("/register")
    public String submitRegisterForm(@ModelAttribute("user") User user) {
        return "register_success";
    }

    @RequestMapping("/login")
    public String showLoginForm(Model model) {
        User user = new User();

        model.addAttribute("user", user);

        return "login_form";
    }

    @PostMapping("/login")
    public String submitLoginForm(@ModelAttribute("user") User user) {
        return "login_success";
    }
}

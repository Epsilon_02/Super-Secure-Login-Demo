<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<!DOCTYPE html>
<html>

<head>
  <meta charset="ISO-8859-1">
  <title>Login Success</title>
    <link href="/css/water.css" rel="stylesheet"/>
  <style type="text/css">
    span {
      display: inline-block;
      width: 200px;
      text-align: left;
    }
  </style>
</head>
<body>
  <h2>Hello ${user.username}!</h2>
  <br />
  <br />
  <a href="/">Go Back!</a>
</body>
</html>

<!DOCTYPE html>
<html>
<head>
  <meta charset="ISO-8859-1">
  <title>Spring Boot Form Handling Example</title>
  <link href="/css/water.css" rel="stylesheet"/>
  <style>
    b {
      font-size: 200%;
    }

    a:nth-of-type(1) {
      padding-right: 30px;
  </style>
</head>
<body>
  <h1><b>S</b>uper <b>S</b>ecure <b>L</b>ogin</h1>
  <a href="/register">Register User</a><a href="/login">Login</a>
</body>
</html>

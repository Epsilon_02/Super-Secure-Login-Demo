<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<!DOCTYPE html>
<html>

<head>
  <meta charset="ISO-8859-1">
  <title>Registration Success</title>
    <link href="/css/water.css" rel="stylesheet"/>
  <style type="text/css">
    span {
      display: inline-block;
      width: 200px;
      text-align: left;
    }
  </style>
</head>
<body>
  <h2>Registration Succeeded!</h2>
  <span>Username:</span><span>${user.username}</span><br />
  <span>Password:</span><span>****</span><br />
  <br />
  <br />
  <a href="/">Go Back!</a>
</body>
</html>

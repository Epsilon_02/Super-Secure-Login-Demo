<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
  <meta charset="ISO-8859-1">
  <title>User Login Form</title>
  <link href="/css/water.css" rel="stylesheet"/>
</head>
<body>
  <h2>User Login</h2>
  <form:form action="login" method="post" modelAttribute="user">
    <form:label path="username">Username:</form:label>
    <form:input path="username" />
    <br />
    <form:label path="password">Password:</form:label>
    <form:password path="password" />
    <br />
    <form:button>Register</form:button>
  </form:form>
  <a href="/register">Go to Register User</a>
</body>
</html>
